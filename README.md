# EEE088F Group 29

A HAT design to be added to the STM32 microcontroller.

## Ambient Light Sensor Description
Our HAT will primarily be an ambient light sensor HAT. This product has a range of potential users, both commercial and private. The primary use for this device will undoubtedly be in the regulation of lighting following the time of day or any change in the amount of light in an area. A simple use for this device would be for a company or an individual to place this device in each room in a house/office and link it to the switch of the lights in the respective rooms, this will save electricity and effort. The device will also be able to be manually overridden by the user simply by putting the switch back off or disconnecting the HAT. A more complex use for this device would be for a user to attach this device to a projector and allow it to control the brightness of the display by measuring the ambient lighting on the area of projection.

Alongside the digital light sensor, we have integrated an analog temperature sensor. We have installed this sensor to fulfill a need for customers using this product in the industrial industry or in areas with extreme weather. The temperature sensor will constantly guage the heat or cold in the room, and will carry the power to switch off the device if the temperature is not between 10-50 degrees celsius - this is to ensure that the HAT is not damaged as this is the optimal temperature range for an AA battery. 


## Contents
This repo contains all files needed to understand the functionality of the HAT as well as have the HAT built. A description of the folder contents can be found below:

* [CAD](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/CAD) - Models and designs associated with the project

* [DOC](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/DOC) - Documentation which may include schematics, datasheets and manuals

* [FRM](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/FRM) - Source code for the HAT's software interface. 

* [PCB](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/PCB) - Contains PCB schematic diagrams 

* [PRD](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/PRD) - Documents relating to cost of HAT and production of HAT

* [SIM](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/SIM) - Contains documents neccessary for the fabrication of the HAT


## Support
Any questions concerning the project can be addressed to the project developers at NKMMOR001@myuct.ac.za; vjykes001@myuct.ac.za 


## Authors and acknowledgment
Project designers are Morris Nkomo and Keshav Vijayanath.


## Project status
Project is currently under development.
